<?php
include ('../config.php');
session_start();
if(isset($_POST['submit'])){
  $name_product        = $_POST['name_product'];
  $categories_product  = $_POST['categories_product'];
  $price               = $_POST['price'];
  $description         = $_POST['description'];
  $product_code        = $_POST['product_code'];
  $weight              = $_POST['weight'];
  $image               = $_FILES['image']['name'];
  $tmp                 = $_FILES['image']['tmp_name'];
  $fotobaru            = date('dmYHis').$image;
  $path                = "imgproduct/".$fotobaru;
  $realpath            = "http://$_SERVER[HTTP_HOST]/babaong/admin/".$path;
  if(move_uploaded_file($tmp, $path)){
    $sql="INSERT INTO product (categories_product,name_product,description,price,image,product_code,weight)VALUES('$categories_product','$name_product','$description','$price','$fotobaru','$product_code','$weight')";
    $result = mysqli_query($conn,$sql);
      if(!$result){
       echo"<script>alert('Tambah Product Gagal !!!'); window.location = 'inputproduct.php' </script>";
      }else{
        echo"<script>alert('Tambah Product Sukses'); window.location = 'inputproduct.php' </script>";
      }
    }else{
      echo"gagal";
    } 
  }
?>



<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baba Ong Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="../img/icon.png">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->

    <?php include "sidebar.php";?>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include "header.php";?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Basic</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Form Input Product</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  <form action="inputproduct.php" enctype="multipart/form-data" method="post" novalidate="novalidate">
                                      <div class="form-group">
                                          <label for="product_code" class="control-label mb-1">Code Product</label>
                                          <input id="product_code" name="product_code" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                      </div>

                                      <div class="form-group">
                                          <label for="namaproduct" class="control-label mb-1">Product Name</label>
                                          <input id="namaproduct" name="name_product" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                      </div>
                                      <div class="form-group">
                                          <label for="categori" class="control-label mb-1">Categori</label>
                                          <div class="form-group">
                                          <select class="custom-select" name="categories_product">
                                              <option selected>Choose...</option>
                                              <option value="arabika">Arabika</option>
                                              <option value="robusta">Robusta</option>
                                             
                                          </select>
                                            </div>
                                      </div>
                                      <div class="form-group has-success">
                                          <label for="desk" class="control-label mb-1">Deskripsi</label>
                                          <input id="desk" name="description" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name on card" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                          <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                       <label>Upload Foto<span class="required">*</span></label><br>
                                      <input class="" id="image" name="image" type="file" value="" required />
                                     </div>
                                      <div class="form-group">
                                          <label for="harga" class="control-label mb-1">Harga</label>
                                          <input id="price" name="price" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="" data-val-cc-number="" autocomplete="">
                                          <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="weight" class="control-label mb-1">Weight</label>
                                          <input id="weight" name="weight" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                      </div>
                                        
                                      </div>
                                      <div>
                                          <button name="submit" type="submit" class="btn btn-lg btn-info btn-block"> 
                                              <span>Simpan</span>
                                              <span style="display:none;">proses...</span>
                                          </button>
                                      </div>
                                  </form>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->

                

                  
                  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
