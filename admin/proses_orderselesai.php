



<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baba Ong Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="../img/icon.png">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->

    <?php include "sidebar.php";?>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include "header.php";?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Basic</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Form Order Success Verification</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                <div class="col-md-6">
                                  <?php
                                    include '../config.php';
                                    $idtrans  = $_GET['idtrans'];
                                    $cek      = mysqli_query($conn, "SELECT *, transaksi.date_order as dOrder FROM transaksi LEFT JOIN pembelian ON transaksi.invoice=pembelian.invoice LEFT JOIN user ON transaksi.id_user=user.id_user WHERE id_transaction='$idtrans'");
                                    // $cek      = mysqli_query($conn, "SELECT *,pembelian.invoice as invoo, pembelian.date_order as tglbeli FROM pembelian LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice LEFT JOIN user ON pembelian.id_user=user.id_user WHERE pembelian.invoice='$inv' ") or die (mysqli_error($conn));
                                    $ambil1 = mysqli_fetch_array($cek);
                                    
                                  ?>
                                  <h4><center>Data Order</center></h4><br>
                                  <table class="table table-responsive">
                                    <tr><td>Name</td><td>:</td><td><?php echo $ambil1['fullname'] ?></td></tr>
                                    <tr><td>No Order</td><td>:</td><td><?php echo $ambil1['invoice'] ?></td></tr>
                                    <tr><td>Date Order</td><td>:</td><td><?php echo date("Y-m-d", strtotime($ambil1['dOrder'])) ?></td></tr>
                                    <tr><td>Status Order</td><td>:</td>
                                        <td><?php 
                                              if ($ambil1['confirmation']==0) {
                                                echo "Menunggu Konfirmasi Admin";
                                              }elseif ($ambil1['confirmation']==1) {
                                                echo "Pesanan Diproses";
                                              }elseif ($ambil1['confirmation']==2) {
                                                echo "Pesanan Selesai/Terkirim";
                                              }elseif ($ambil1['confirmation']==3) {
                                                echo "Pesanan Dibatalkan";
                                              }

                                            ?>
                                          
                                        </td>
                                    </tr>
                                    <tr><td>Evidence Of Transfer</td><td>:</td><td><img src="../admin/images/bukti/<?php echo $ambil1['foto'] ?>" alt="img-responsive" class="img-responsive" width="100px" ></td></tr>
                                  </table>    
                                </div>

                                <div class="col-md-6">
                                  <?php
                                    include '../config.php';
                                    $idtrans  = $_GET['idtrans'];
                                    $sql2 = mysqli_query($conn, "SELECT *,pembelian.total_price as tot_satuan FROM pembelian LEFT JOIN product ON pembelian.product_code=product.product_code LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice WHERE transaksi.id_transaction='$idtrans' ");
                                    
                                  ?>
                                  <h4><center>List Order</center></h4>
                                  <br>
                                  <table class="table table-striped">
                                    <thead>
                                    <tr>
                                      <td>No</td>
                                      <td>id Product</td>
                                      <td>Product Name</td>
                                      <td>Price</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $no=1; while ($fetchsql2 = mysqli_fetch_array($sql2)) { ?>
                                    <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $fetchsql2['product_code'] ?></td>
                                      <td><?php echo $fetchsql2['name_product'] ?></td>
                                      <td><?php echo $fetchsql2['tot_satuan'] ?></td>

                                    </tr>
                                    <?php $no++; } ?>

                                    </tbody>

                                  </table>   
                                </div>
                                <div class="col-md-12">
                                  <center>
                                    <form method="post">
                                      <input type="text" name="idtrans" value="<?php echo $idtrans ?>" hidden>
                                      <label>Click 'Finish Order' button if the order has been sent to customer</label><br>
                                      <button class="btn btn-success" name="orderok">Finished Order</button>
                                      <button class="btn btn-danger" name="orderbatal">Cancel Order</button>
                                    </form>
                                  </center>
                                </div>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->

                  <?php 
                  if (isset($_POST['orderok'])) {
                    $trans = $_POST['idtrans'];
                    $update = mysqli_query($conn, "UPDATE transaksi SET confirmation='2' WHERE id_transaction='$idtrans' ");
                    if ($update) {
                      echo "<script>alert('Update Data Success! Order Finished');</script>";
                      echo "<script>location='ordersukses.php';</script>";
                    }
                  }elseif (isset($_POST['orderbatal'])) {
                    $trans = $_POST['idtrans'];
                    $update = mysqli_query($conn, "UPDATE transaksi SET confirmation='3' WHERE id_transaction='$idtrans' ");
                    if ($update) {
                      echo "<script>alert('Update Data Success! Order Deleted');</script>";
                      echo "<script>location='orderbatal.php';</script>";
                    }
                  }

                   ?>

                  
                  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
