<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baba Ong Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="../img/icon.png">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="DataTables/dataTables.min.css">

    <script src="DataTables/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->

    <?php include "sidebar.php";?>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include "header.php";?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Basic</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Form Order Proses</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                 <table id="example" class="table">
  <thead>
    <tr>
      <th >No</th>
      <th >Id Trans</th>
      <th >Buyer Name</th>
      <th >Address</th>
      <th >Total</th>
      <th >EOT</th>
      <th width="10%">Action</th>
      
    </tr>
    </thead>

    <tbody>
    <?php
    include "../config.php";
    $no=1;
    $product = mysqli_query($conn, "SELECT * FROM transaksi LEFT JOIN pembelian ON transaksi.invoice=pembelian.invoice LEFT JOIN user ON transaksi.id_user=user.id_user WHERE transaksi.confirmation='1' GROUP BY transaksi.invoice");
    while($row = mysqli_fetch_array($product)){
      ?>
      <tr>
      <td><?php echo $no;?></td>
      <td><?php echo $row['id_transaction']?></td>
      <td><?php echo $row['buyer_name']?></td>
      <td><?php echo $row['address']?></td>
      <td>Rp. <?php echo number_format($row['total_price']) ?></td>
      <td><img class="img-responsive" height="70px" src="images/bukti/<?php echo $row['foto']?>"></td>
      <td>
        <center>
        <a href="proses_orderselesai.php?idtrans=<?php echo $row['id_transaction'] ?>" class="btn btn-info btn-sm btn-block" title="">Detail Order</a>
        
        
        </center>

      </td>
        <?php include 'modalorder.php'; ?>
      </tr>
      <?php $no++;

    }
    ?>

 </tbody>
</table>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
    
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->
<script>
      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
