<!DOCTYPE html>
<html lang="en">
<head>

  <!-- start: Meta -->
  <meta charset="utf-8">
  <title>Laporan Transaksi</title> 
  <meta name="description" content="Undecimo Bootstrap Theme is incredibly powerfull and responsive template"/>
  <meta name="keywords" content="Bootstrap Theme, Bootstrap Template, Bootstrap, Responsive Theme, Responsive Template, Retina Display, Clear Design, Glyphicons, LayerSlider, FlexSlider, Font Awesome, Icons, Portfolio, Business, WrapBootstrap, Responsive" />
  <meta name="author" content="Łukasz Holeczek from creativeLabs"/>
  <!-- end: Meta -->
  
  <!-- start: Mobile Specific -->
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- end: Mobile Specific -->
  
  <!-- start: Facebook Open Graph -->
  <meta property="og:title" content=""/>
  <meta property="og:description" content=""/>
  <meta property="og:type" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:image" content=""/>
  <!-- end: Facebook Open Graph -->

    <!-- start: CSS -->
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/layerslider.css" rel="stylesheet" type="text/css">
  <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
  

</head>

<body onload="window.print()">

<div class="panel panel-default">
  <div class="panel-body">
    <div class="row-table-bordered">
            
          <div class="col-md-2">
              <img src="../img/logo_babaong.png" class="img-responsive pull-left" style="max-height:150px;display:inline">
          </div>

                <font size="6"><b><p class="text-center">BABA ONG COFFEE</p></b></font>
                <font size="3"><b><p class="text-center">Jalan Raya Panggung No.3, Jatibening, Pondok Gede, Kota Bekasi</p></b></font>
                <b><p class="text-center"> </p></b>
<br>
  <style type="text/css">
      hr.style2 {
      border-top: 3px double #8c8b8b;
      }
    </style>
  
  <hr class="style2">



                                    <?php
                                    include '../config.php';
                                    $idtrans  = $_GET['idtrans'];
                                    $cek      = mysqli_query($conn, "SELECT *, transaksi.date_order as dOrder FROM transaksi LEFT JOIN pembelian ON transaksi.invoice=pembelian.invoice LEFT JOIN user ON transaksi.id_user=user.id_user WHERE id_transaction='$idtrans'");
                                    // $cek      = mysqli_query($conn, "SELECT *,pembelian.invoice as invoo, pembelian.date_order as tglbeli FROM pembelian LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice LEFT JOIN user ON pembelian.id_user=user.id_user WHERE pembelian.invoice='$inv' ") or die (mysqli_error($conn));
                                    $ambil1 = mysqli_fetch_array($cek);
                                    
                                  ?>
                                  <h4><center>Data Order</center></h4><br>
                                  <table class="table table-responsive" align="center">
                                    <tr><td>Name</td><td>:</td><td><?php echo $ambil1['fullname'] ?></td></tr>
                                    <tr><td>No Order</td><td>:</td><td><?php echo $ambil1['invoice'] ?></td></tr>
                                    <tr><td>Date Order</td><td>:</td><td><?php echo date("Y-m-d", strtotime($ambil1['dOrder'])) ?></td></tr>
                                    <tr><td>Status Order</td><td>:</td>
                                        <td><?php 
                                              if ($ambil1['confirmation']==0) {
                                                echo "Menunggu Konfirmasi Admin";
                                              }elseif ($ambil1['confirmation']==1) {
                                                echo "Pesanan Diproses";
                                              }elseif ($ambil1['confirmation']==2) {
                                                echo "Pesanan Selesai/Terkirim";
                                              }elseif ($ambil1['confirmation']==3) {
                                                echo "Pesanan Dibatalkan";
                                              }

                                            ?>
                                          
                                        </td>
                                    </tr>
                                    <tr><td>Evidence Of Transfer</td><td>:</td><td>
                                    <?php 
                                    if (isset($ambil1['foto'])) { ?>
                                      <a href="images/bukti/<?php echo $ambil1['foto']?>">
                                      <img src="images/bukti/<?php echo $ambil1['foto'] ?>" alt="img-responsive" class="img-responsive" width="100px" ></a>
                                   <?php }else{
                                   ?>
                                    -
                                   <?php } ?>


                                    </td></tr>
                                  </table>    
                                </div>

                                <div class="col-md-6">
                                  <?php
                                    include '../config.php';
                                    $idtrans  = $_GET['idtrans'];
                                    $subtotal = floatval("");
                                    $sql2 = mysqli_query($conn, "SELECT *,pembelian.total_price as tot_satuan FROM pembelian LEFT JOIN product ON pembelian.product_code=product.product_code LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice WHERE transaksi.id_transaction='$idtrans' ");
                                    
                                  ?>
                                  <h4><center>List Order</center></h4>
                                  <br>
                                  <table class="table table-condensed" align="center">
                                    <div class="container">

                                    <thead>
                                    <tr>
                                      <td width="15%">No</td>
                                      <td width="35%">ID Product</td>
                                      <td width="40%">Product Name</td>
                                      <td width="25%">Price</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
                                      $toot = array();
                                      $no=1; while ($fetchsql2 = mysqli_fetch_array($sql2)) { ?>
                                    <tr>
                                      <?php $toot[] = $fetchsql2['tot_satuan']; ?>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $fetchsql2['product_code'] ?></td>
                                      <td><?php echo $fetchsql2['name_product'] ?></td>
                                      <td ><?php echo $fetchsql2['tot_satuan'] ?></td>

                                   

                                    </tr>
                                    <?php $no++; } ?>

                                  </tbody>
                                  <tr>
                                    <td colspan="3" align="right">
                                    Total Harga Rp. <td colspan="1" align="right"><?php echo number_format(array_sum($toot)); ?></td>  
                                  </td>
                                  </tr>
                                </div>

                                    </table>

                                    <br>
                                    <br>




</body>
</html>