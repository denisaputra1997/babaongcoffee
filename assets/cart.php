
<?php
session_start();
?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
   <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="../style.css">
   <link rel="icon" type="image/png" href="../img/icon.png">
   <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.min.css">


   <link rel="stylesheet" type="text/css" href="DataTables/dataTables.min.css">

    <script src="DataTables/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <title>Baba Ong Coffee</title>
  </head>
  <body id="page-top">

<!-- Navigation Bar -->

  <nav class="navbar navbar-expand-lg navbar-light bg-white  sticky-top" id="mainNav">
      <div class="container">
  <a class="navbar-brand" href="../index.php"> <img src="../img/logo_babaong.png"></a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../index.php#beranda">BERANDA <span class="sr-only">(current)</span></a> </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUK </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../catalog_arabica.php">ARABIKA</a>
          <a class="dropdown-item" href="../catalog_robusta.php">ROBUSTA</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../index.php#profile">PROFILE</a>
      </li>
    </ul>
    
    <ul class="navbar-nav">
         <?php

         if (isset($_SESSION['fullname'])){ ?>
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-1x fas fa-user">&nbsp;</i>
              <?= $_SESSION['fullname']; ?>

              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="cart.php">Keranjang</a>  
              <a class="dropdown-item" href="detail_order.php">Pesanan Saya</a>
              <a class="dropdown-item" href="logout.php">Keluar</a>

        </div>
              <?php
         }else {
          ?>
      <li class="nav-item">
         <a class="nav-link" href="assets/login.php"><i class="fa-1x fas fa-user">&nbsp;</i>MASUK</a>
        <?php } ?>
        </a>
      </li>

      
     
    </ul> 
    
  </div>
  </div>
</nav>

<!-- content checkout -->

<form method="post" action="cart_proses.php" enctype="multipart/form-data">
<div class="container">
<div class="table-responsive">

  <table class="table table-bordered" id="tbTransaksi" name="tbTransaksi">


    <tr>

      <td width="15%">Kode Produk</td>
      <td width="">Nama Produk</td>
      <td width="10%">Banyaknya</td>
      <td width="15%">Harga</td>
      <td width="15%">Total Harga</td>
      

    </tr>

      <?php
          include '../config.php';


          $fullname = $_SESSION['fullname'];
          // echo "$_SESSION[id_user]";

          $query="SELECT * FROM user ORDER BY id_user ";
          $tampil=mysqli_query($conn,$query)or die(mysqli_error($conn));
          if ($fullname) {
          while ($data=mysqli_fetch_array($tampil));
          }else{
            echo "<script> window.location.href='login.php';</script>";
          }
          ?>


          <?php
          $subtotal = floatval("");
          $sql="SELECT * FROM pembelian,user WHERE pembelian.id_user=user.id_user AND user.fullname='$_SESSION[fullname]' AND fixed='0' ORDER BY id_order";
          $value=mysqli_query($conn,$sql)or die(mysql_error($conn));
          while ($row=mysqli_fetch_array($value)) {
            # code...
          
          ?>


    <tr>
      
      <td><?php echo $row['product_code']; ?></td>
      
      <input type="hidden" name="product_code" value="<?php echo $row['product_code']; ?>">
      
      <td><?php echo $row['name_product']; ?></td>
      
      <td><?php echo $row['qty']; ?></td>
      
      <td>Rp. <?php echo number_format($row['price'], 2); ?></td>
      
      <td>Rp. <?php echo number_format($row['total_price'], 2); ?></td>


      


    </tr>
        
    <tr>
            <?php

            $total = $row['total_price'];
            $subtotal += floatval($total);
            
            ?>
            
    </tr>

     <?php
     }

     ?>
           
            <td colspan="4" align="right">Jumlah Total</td>
            <td >Rp. <?php echo number_format($subtotal, 2); ?></td>
             <input type="hidden" name="total" value="<?php echo $subtotal; ?> ">
     
       



  </table>

  <table>
    
    <div class="card-body">
      <!-- Credit Card -->
     <!--  <div id="pay-invoice"> -->
            
      <!-- <div class="row">

      <div class=" col-md-6" position = "left">

          <div class="card-body">
              <div class="form-group">
                      <label for="buyer" class="control-label mb-1">Username</label>
                      <input id="buyer" name="buyer" type="text" class="form-control" aria-required="true" aria-invalid="false" value="<?php echo $_SESSION['fullname']; ?>" reqired>
                  </div>

                  <div class="form-group">
                      <label for="address" class="control-label mb-1">Address</label>
                      <input id="address" name="address" type="text" class="form-control" value="" required>
                  </div>

               

                
                </div>

              </div>
            

                <div class="col-md-6" position = "right">

          <div class="card-body">
              <div class="form-group">
                      <label for="email" class="control-label mb-1">Email</label>
                      <input id="email" name="email" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required>
                  </div>

                  <div class="form-group">
                      <label for="Phone" class="control-label mb-1">Phone</label>
                      <input id="Phone" name="Phone" type="text" class="form-control" value="" required>
                  </div>
 -->


                
            <div class=" form-group float-right"  >

              <input name="inputBuy" type="submit" class="btn btn-outline-secondary mt-2" value="Lanjutkan!" width="">
            </div>
               </div>

              <!-- </div>
             
            </div>

          
            </div> -->

          
           <!--  </div> -->


                           
    

  </table>

</div>
</div>
  
</form>









    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script type="text/javascript" src="../js/jquery.min.js"></script>
   <script type="text/javascript" src="../js/popper.min.js"></script>
   <script type="text/javascript" src="../js/bootstrap.min.js"></script>

   <div class="card-footer text-mute fixed-bottom">
     <div class="row">
     <div class="col-sm-8" style= "text-align: left; font-size: 12px; text-decoration: none;">
      
      <a href="#" style="color: #808080">email@babaongcoffee.com</a>&nbsp;&nbsp;&nbsp;

     </div>
     <div class="col-sm-4" style="text-align: right; color: #808080; font-size: 12px;">
      2019 ® Baba Ong Coffee

     </div>
     </div>
   </div>
  <script>
      $(document).ready(function() {
    $('#example').DataTable();
} );


    </script>
  </body>
</html>