<?php

session_start();
include '../config.php';
?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
   <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="../style.css">
   <link rel="icon" type="image/png" href="../img/icon.png">
   <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.min.css">


   <link rel="stylesheet" type="text/css" href="DataTables/dataTables.min.css">

    <script src="DataTables/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <title>Baba Ong Coffee</title>
  </head>
  <body id="page-top">

<!-- Navigation Bar -->

  <nav class="navbar navbar-expand-lg navbar-light bg-white  sticky-top" id="mainNav">
      <div class="container">
  <a class="navbar-brand" href="../index.php"> <img src="../img/logo_babaong.png"></a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../index.php#beranda">BERANDA <span class="sr-only">(current)</span></a> </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUK </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../catalog_arabica.php">ARABIKA</a>
          <a class="dropdown-item" href="../catalog_robusta.php">ROBUSTA</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../index.php#profile">PROFILE</a>
      </li>
    </ul>
    
    <ul class="navbar-nav">
         <?php

         if (isset($_SESSION['fullname'])){ ?>
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-1x fas fa-user">&nbsp;</i>
              <?= $_SESSION['fullname']; ?>

              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="cart.php">Keranjang</a>
              <a class="dropdown-item" href="detail_order.php">Pesanan Saya</a>
              <a class="dropdown-item" href="logout.php">keluar</a>

        </div>
              <?php
         }else {
          ?>
      <li class="nav-item">
         <a class="nav-link" href="assets/login.php"><i class="fa-1x fas fa-user">&nbsp;</i>Masuk</a>
        <?php } ?>
        </a>
      </li>

      
     
    </ul> 
    
  </div>
  </div>
</nav>

<div class="container ">
  <br><br><br>
  <div class="row">
    <div class="col-md-3">
      <form method="post" enctype="multipart/form-data">
        <div class="form-row">
          <div class="form-group col-md-12">
            <label>No. Pesanan</label>
            <select name="no_order" class="form-control">
              <option value="0">--Pilih Pesanan--</option>
              <?php 
              
                $sql    = mysqli_query($conn, "SELECT * FROM pembelian WHERE  id_user='$_SESSION[id_user]' GROUP BY invoice ") or die (mysqli_error($conn));
                while ($fetch  = mysqli_fetch_array($sql)) { ?>
                  <!-- <input type="text" name="" > -->
                    <option value="<?php echo $fetch['invoice'] ?>"><?php echo $fetch['invoice'] ?></option>
                  <?php }  ?>
            </select>
          </div>
          <div class="form-group">
            <button name="check" class="btn btn-info btn-block">Cek Pesanan</button>
          </div>
        </div>
    </div>
    <div class="col-md-9">
      <h5><center>Pesanan Anda</center></h5>
      <?php 
        if (isset($_POST["check"])){
          $inv = $_POST['no_order'];
          if ($inv==0) { ?>
            <h3><center>Data No Order Belum Dipilih</center></h3>
          <?php }else{

          // echo "$inv";
          $cek    = mysqli_query($conn, "SELECT *,pembelian.invoice as invoo, pembelian.date_order as tglbeli FROM pembelian LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice LEFT JOIN user ON pembelian.id_user=user.id_user WHERE pembelian.invoice='$inv' ") or die (mysqli_error($conn));
          $ambil1 = mysqli_fetch_array($cek);
          // echo $ambil1["id_user"];
          // echo "$_POST[no_order]";
      ?>
        <table class="table table-responsive">
          <tr><td>Nama Pemesan</td><td>:</td><td><?php echo $ambil1['fullname'] ?></td></tr>
          <tr><td>No. Pesanan</td><td>:</td><td><?php echo $ambil1['invoo'] ?></td></tr>
          <tr><td>Tanggal Pemesanan</td><td>:</td><td><?php echo date("Y-m-d", strtotime($ambil1['tglbeli'])) ?></td></tr>
          <tr><td>Status Pesanan</td><td>:</td>
              <td><?php 
                    if ($ambil1['confirmation']==0) {
                      echo "Menunggu Konfirmasi Admin";
                    }elseif ($ambil1['confirmation']==1) {
                      echo "Pesanan Diproses";
                    }elseif ($ambil1['confirmation']==2) {
                      echo "Pesanan Selesai/Terkirim";
                    }elseif ($ambil1['confirmation']==3) {
                      echo "Pesanan Dibatalkan";
                    }

                  ?>
                
              </td>
          </tr>
          <tr><td>Bukti Transfer</td><td>:</td><td>
            <?php 
              if (isset($ambil1['foto'])) { ?>
                <a href="../admin/images/bukti/<?php echo $ambil1['foto']?>">
                <img src="../admin/images/bukti/<?php echo $ambil1['foto'] ?>" alt="img-responsive" class="img-responsive" width="100px" ></a>
             <?php }else{
             ?>
              -
             <?php } ?>
          </td></tr>
        </table>


        <?php
          $cektrans = mysqli_query($conn, "SELECT * FROM transaksi WHERE invoice='$inv' ");
          $itung = mysqli_num_rows($cektrans);
          if ($itung>0):
            $sql2 = mysqli_query($conn, "SELECT *,pembelian.total_price as tot_satuan FROM pembelian LEFT JOIN product ON pembelian.product_code=product.product_code LEFT JOIN transaksi ON pembelian.invoice=transaksi.invoice WHERE pembelian.invoice='$inv' ");

        ?>
        <br>
        <h4><center>List Order</center></h4>
            <table class="table">
              <thead>
              <tr>
                <td>No</td>
                <td>Id Produk</td>
                <td>Nama Produk</td>
                <td>Harga</td>
              </tr>
              </thead>
              <tbody>
                <?php $no=1; while ($fetchsql2 = mysqli_fetch_array($sql2)) { ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $fetchsql2['product_code'] ?></td>
                <td><?php echo $fetchsql2['name_product'] ?></td>
                <td><?php echo $fetchsql2['tot_satuan'] ?></td>

              </tr>
              <?php $no++; } ?>

              </tbody>

            </table>
          <?php elseif ($itung==0):?>
            <center>Belum ada data Transaksi</center>
            <form method="post" enctype="multipart/form-data" action="bukti_proses.php">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label>Input Bukti Transfer</label>
                  <input type="file" name="bukti" class="form-control">
                </div>
                <div class="form-group col-md-12">
                  <input type="text" name="inv" value="<?php echo $inv ?>" hidden>
                  <input type="text" name="id_user" value="<?php echo $ambil1['id_user'] ?>" hidden>
                </div>
                <div class="form-group">
                  <button class="btn btn-info btn-block" name="inputdata">Unggah</button>
                </div>
              </div>
            </form>
        <?php endif; ?>

                   


      <?php } } ?>

    </div>
  </div>
</div>
<br>
<br>
<br>
<br>

<?php
  if (isset($_POST["inputdata"])) {
    $foto = $_FILES['bukti']['name'];
    $tmpfile = $_FILES['bukti']['tmp_name'];
    $namafoto = date('dmYHis').$foto;
    $path = "../admin/images/bukti/".$namafoto;
    $inv = $_POST['inv'];
    $id_user = $_POST['id_user'];

    $cekuser = mysqli_query($conn, "SELECT * FROM pembelian LEFT JOIN user ON pembelian.id_user=user.id_user WHERE pembelian.invoice='$inv'");
    $fetchuser    = mysqli_fetch_array($cekuser);
    $iduser       = $fetchuser['id_user'];
    $nama         = $fetchuser['fullname'];
    $date_order   = date("Y-m-d", strtotime($fetchuser["date_order"]));
    $address      = $fetchuser['address'];
    $confirmation = 0;

    $cektotal = mysqli_query($conn, "SELECT * FROM pembelian WHERE invoice='$inv' ");
    // $ada = mysqli_num_rows($cektotal);
    // if ($ada>0) {
    //   echo "ADA NIH";
    // }else{
    //   echo "GAADA NIH";
    // }
    $totalharga = array();
    while ($fetchtotal = mysqli_fetch_array($cektotal)) {
      $totalharga[]= $fetchtotal['total_price'];
    }
    $tot = array_sum($totalharga);
    // echo "<h1>$semua</h1>";

    if (move_uploaded_file($tmpfile, $path)) {
      $add = mysqli_query($conn, "INSERT INTO transaksi(id_user, date_order, buyer_name, address, confirmation, invoice, total_harga, foto) VALUES ('$iduser','$date_order', '$nama', '$address', '$confirmation', '$inv', '$tot', '$namafoto') ") or die (mysqli_error($conn));
      if ($add) {
        echo "<script>alert('BERHASIL UPDATE!');</script>";
        echo "<script>location='detail_order.php'</script>";
      }


      
  }else{
      echo "<script>alert('GAGAL!');</script>";
  }


}



      ?>






<!-- Your Order Detail -->

<!-- <form method="post" action="checkout_proses.php" enctype="multipart/form-data">
<div class="container ">
<div class="table-responsive">

<h5 align="center">Your Order Detail</h5>
<hr width="18%" size="50px" color="black">


<div class="container col-md-8 " >

  <div class="row">

<div class="col col-md-2 ">

<p>Name
<p>Email 
<p>Id Order
<p>Date Order
<p>Address
<p>Jne
<p>Jne Price
<p>Total
<p>Total Price

</div> 

<div class="col col-md-4 ">

<p>:&nbsp;Name
<p>:&nbsp;Email 
<p>:&nbsp;Id Order
<p>:&nbsp;Date Order
<p>:&nbsp;Address
<p>:&nbsp;Jne
<p>:&nbsp;Jne Price
<p>:&nbsp;Total
<p>:&nbsp;Total Price

</div>  

</p>

</div>







</div>
</div>
</form>
 -->


<script type="text/javascript" src="../js/jquery.min.js"></script>
   <script type="text/javascript" src="../js/popper.min.js"></script>
   <script type="text/javascript" src="../js/bootstrap.min.js"></script>

   <div class="card-footer text-mute fixed-bottom">
     <div class="row">
     <div class="col-sm-8" style= "text-align: left; font-size: 12px; text-decoration: none;">
      
      <a href="#" style="color: #808080">email@babaongcoffee.com</a>&nbsp;&nbsp;&nbsp;

     </div>
     <div class="col-sm-4" style="text-align: right; color: #808080; font-size: 12px;">
      2019 ® Baba Ong Coffee

     </div>
     </div>
   </div>
  <script>
      $(document).ready(function() {
    $('#example').DataTable();
} );


    </script>
  </body>
</html>