<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
   <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="../style.css">
   <link rel="icon" type="image/png" href="../img/icon.png">
   <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.min.css">

    <title>Baba Ong Coffee</title>
  </head>
  <body id="page-top">

<!-- Navigation Bar -->

  <nav class="navbar navbar-expand-lg navbar-light bg-white  sticky-top" id="mainNav">
      <div class="container">
  <a class="navbar-brand" href="../index.php"> <img src="../img/logo_babaong.png"></a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../index.php#beranda">BERANDA <span class="sr-only">(current)</span></a> </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUK </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../catalog_arabica.php">ARABIKA</a>
          <a class="dropdown-item" href="../catalog_robusta.php">ROBUSTA</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../index.php#profile">PROFILE</a>
      </li>
    </ul>
     
    </ul>
  </div>
  </div>
</nav>

<!--form registrasi -->
<div class="container" style="width: 600px; padding: 10px;">
 <div class="row">
    <div class="col-sm-12 align-center">
<form method="post" action="registrasi_proses.php">
  <div class="form-header">
  <div class="form-group">
    <label for="formGroupExampleInput">Nama Lengkap</label>
    <input type="text" name="fullname" class="form-control" id="formGroupExampleInput" placeholder="Enter Your Name" required>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Tanggal Lahir</label> 
<p>  

    <select class="custom-select col-sm-3 col-md-3 col-lg-3 mb-1" name="yyyy" id="inputGroupSelect01" required>
      
      <?php
          for($year = date('Y'); $year >=1980; $year--) {
            echo '<option value="'. $year .'">'. $year .'</option>';
        }

          ?>

    </select>
    <select class="custom-select col-sm-3 col-md-3 col-lg-3 mb-1" name="mm" id="inputGroupSelect01" required>
      
      <?php
          for($month = 1; $month <= 12; $month ++){
            if($month < 10){
              echo '<option value="0'.$month.'">0'.$month.'</option>';
          }
              else{
              echo'<option value="'.$month.'">'.$month.'</option>';
            }
        }

          ?>

    </select>

    <select class="custom-select col-sm-3 col-md-3 col-lg-3 mb-1" name="dd" id="inputGroupSelect01" required>
      <?php
          for($day = 1; $day <= 31; $day ++){
            if($day < 10){
              echo '<option value="0'.$day.'">0'.$day.'</option>';
          }
              else{
              echo'<option value="'.$day.'">'.$day.'</option>';
            }
        }

          ?>

    </select>
  </div>
</p>
   <!--  <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</button>  -->

    
</div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Alamat Lengkap</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" name="address" placeholder="Address" required>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Email</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" name="email" placeholder="Example@email.com" required>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Password</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" name="password" placeholder="Password" required>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Re-password</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" name="repassword" placeholder="Re-password" required>
  </div>


    <input type="hidden" class="form-control" id="formGroupExampleInput2" name="hash" >

  
    <input type="hidden" class="form-control" id="formGroupExampleInput2" name="active" >

  <div class="modal-footer">
       <input type="submit" class="btn btn-secondary" data-dismiss="modal" name="submit" value="Daftar"></button>
       <a href="../index.php" role="button" class="btn btn-secondary" data-dismiss="modal">Kembali</a></button>
</div>



</form>
</div>
</div>
</div>
</div>
<!-- End Modal Login -->




<div class="jumbotron-fluid bg-light" style="padding: 20px">
  <div class="container">
    <h5 align="center"></h3>
  </div>
</div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script type="text/javascript" src="../js/jquery.min.js"></script>
   <script type="text/javascript" src="../js/popper.min.js"></script>
   <script type="text/javascript" src="../js/bootstrap.min.js"></script>

   <div class="card-footer text-mute fixed-bottom">
     <div class="row">
     <div class="col-sm-8" style= "text-align: left; font-size: 12px; text-decoration: none;">
      
      <a href="#" style="color: #808080">email@babaongcoffee.com</a>&nbsp;&nbsp;&nbsp;

     </div>
     <div class="col-sm-4" style="text-align: right; color: #808080; font-size: 12px;">
      2019 ® Baba Ong Coffee

     </div>
     </div>
   </div>
  
  </body>
</html>