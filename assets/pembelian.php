
<?php 
include '../config.php';
session_start();




if(isset($_POST["add_to_cart"]))
{
  if(isset($_SESSION["shopping_cart"]))
  {
    $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
    if(!in_array($_GET["id"], $item_array_id))
    {
      $count = count($_SESSION["shopping_cart"]);
      $item_array = array(
        'item_id'     =>  $_GET["id"],
        'item_name'     =>  $_POST["hidden_name"],
        'item_price'    =>  $_POST["hidden_price"],
        'product_code'  => $_POST["product_code"],
        'item_quantity'   =>  $_POST["quantity"]
      );
      $_SESSION["shopping_cart"][$count] = $item_array;
    }
    else
    {
      echo '<script>alert("Telah ditambahkan")</script>';
    }
  }
  else
  {
    $item_array = array(
      'item_id'     =>  $_GET["id"],
      'item_name'     =>  $_POST["hidden_name"],
      'item_price'    =>  $_POST["hidden_price"],
      'product_code'  => $_POST["product_code"],
      'item_quantity'   =>  $_POST["quantity"]
    );
    $_SESSION["shopping_cart"][0] = $item_array;
  }
}

if(isset($_GET["action"]))
{
  if($_GET["action"] == "delete")
  {
    foreach($_SESSION["shopping_cart"] as $keys => $values)
    {
      if($values["item_id"] == $_GET["id"])
      {
        unset($_SESSION["shopping_cart"][$keys]);
        echo '<script>alert("Telah dihapus")</script>';
        echo '<script>window.location="pembelian.php"</script>';
      }
    }
  }
}




?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
   <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="../style.css">
   <link rel="icon" type="image/png" href="../img/icon.png">
   <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.min.css">


   <link rel="stylesheet" type="text/css" href="DataTables/dataTables.min.css">

    <script src="DataTables/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <title>Baba Ong Coffee</title>
  </head>
  <body id="page-top">

<!-- Navigation Bar -->

  <nav class="navbar navbar-expand-lg navbar-light bg-white  sticky-top" id="mainNav">
      <div class="container">
  <a class="navbar-brand" href="../index.php"> <img src="../img/logo_babaong.png"></a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../index.php#beranda">BERANDA <span class="sr-only">(current)</span></a> </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUK </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../catalog_arabica.php">ARABIKA</a>
          <a class="dropdown-item" href="../catalog_robusta.php">ROBUSTA</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../index.php#profile">PROFILE</a>
      </li>
    </ul>
     
  <ul class="navbar-nav">
         <?php
         if (isset($_SESSION['fullname'])){ ?>
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-1x fas fa-user">&nbsp;</i>
              <?= $_SESSION['fullname']; ?>

              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="cart.php">Keranjang</a> 
              <a class="dropdown-item" href="detail_order.php">Pesanan Saya</a>
              <a class="dropdown-item" href="logout.php">Keluar</a>

        </div>
              <?php
         }else {
          ?>
      <li class="nav-item">
         <a class="nav-link" href="assets/login.php"><i class="fa-1x fas fa-user">&nbsp;</i>MASUK</a>
        <?php } ?>
        </a>
      </li>

       
     
    </ul>

  </div>
  </div>
</nav>
<form method="post" action="pembelian_proses.php" enctype="multipart/form-data">
  <form role="form">
<div class="container">
<div class="table-responsive">
        <table class="table table-bordered">
          <tr>
            <th width="40%">Nama Produk</th>
            <th width="10%">Banyaknya</th>
            <th width="20%">Harga</th>
            <th width="15%">Total Harga</th>
            <th width="5%">Keterangan</th>
          </tr>
          <?php
          include '../config.php';
         
          $fullname = $_SESSION['fullname'];
          $query="SELECT * FROM user WHERE fullname = '$fullname' ";
          $tampil=mysqli_query($conn,$query)or die(mysqli_error($conn));
          if ($fullname) {
          while ($data=mysqli_fetch_array($tampil)) { ?>
            <input type="hidden" name="id_user" value="<?php echo $data['id_user']; ?>">

          <?php
          }
          }else{
            echo "<script> window.location.href='login.php';</script>";
          }
          ?>

          <?php
          
          if(!empty($_SESSION["shopping_cart"]))
          {
            $total = 0;
            foreach($_SESSION["shopping_cart"] as $keys => $values)
            {
          ?> 




          <tr>

            
            <input type="hidden" name="product_code[]" value="<?= $values["product_code"]; ?>">
            <input type="hidden" name="date[]" value="<?= date('d-m-Y', strtotime($data['tanggal'])); ?>">
            <input type="hidden" name="item_name[]" value="<?= $values["item_name"]; ?>">
            <input type="hidden" name="item_quantity[]" value="<?= $values["item_quantity"]; ?>">
            <input type="hidden" name="item_price[]" value="<?= $values["item_price"]; ?>">
            <input type="hidden" name="total[]" value="<?= $values["item_quantity"] * $values["item_price"] ; ?>">
            
            <td><?php echo $values["item_name"]; ?></td>
            <td><?php echo $values["item_quantity"]; ?></td>
            <td>Rp. <?php echo $values["item_price"]; ?></td>
            <td>Rp. <?php echo $values["item_quantity"] * $values["item_price"] ;?></td>
            <td><a href="pembelian.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Hapus</span></a></td>
          </tr>
          <?php
              $total = $total + ($values["item_quantity"] * $values["item_price"]);
            }

          ?>
          <tr>
            <td colspan="3" align="right">Jumlah Total</td>
            <td align="right">Rp. <?php echo $total ; ?></td>
            <td></td>
          </tr>

          <tr>

            <div class="form-group">
            <td align="right" colspan="5"><button type="submit" name="submit" class="btn btn-outline-secondary">CEK</button></td>
            </div>
          </tr>

          <?php
          
            
          }
        
        
          ?>
            
        </table>
      </div>
      </div>
      </form>
    </form>








    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script type="text/javascript" src="../js/jquery.min.js"></script>
   <script type="text/javascript" src="../js/popper.min.js"></script>
   <script type="text/javascript" src="../js/bootstrap.min.js"></script>

   <div class="card-footer text-mute fixed-bottom">
     <div class="row">
     <div class="col-sm-8" style= "text-align: left; font-size: 12px; text-decoration: none;">
      
      <a href="#" style="color: #808080">email@babaongcoffee.com</a>&nbsp;&nbsp;&nbsp;

     </div>
     <div class="col-sm-4" style="text-align: right; color: #808080; font-size: 12px;">
      2019 ® Baba Ong Coffee

     </div>
     </div>
   </div>
  <script>
      $(document).ready(function() {
    $('#example').DataTable();
} );


    </script>
  </body>
</html>